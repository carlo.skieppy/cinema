<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Carbon\CarbonImmutable;
use Exception;
use Validator;
use Auth;

class BookingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }


    ////////////////////////////////////////////////////////////
    // List all films
    public function films(Request $request, $cinema_id = null)
    {
        $cinemas = DB::table('cinemas')->select('id', 'name', 'web_name')->get();

        // Using Laravel collections as to retrieve the current cinema id and name
        $current_cinema = $cinemas->where('id', $cinema_id)->first();

        $films = DB::table('films')
                    ->select('films.id', 'films.name', 'films.web_name', 'films.duration')
                    ->get();

        return view('bookings.films', [
            'current_cinema' => $current_cinema,
            'cinemas' => $cinemas,
            'films' => $films,
        ]);
    }

    ////////////////////////////////////////////////////////////
    // Show the film details and booking times
    public function film(Request $request, $cinema_web_name = null, $film_web_name = null)
    {
        $cinemas = DB::table('cinemas')->select('id', 'name', 'web_name')->get();

        // Using Laravel collections as to retrieve the current cinema id and name
        $current_cinema = $cinemas->where('web_name', $cinema_web_name)->first();

        $film = DB::table('films')
                    ->where('web_name', $film_web_name)
                    ->select('films.id', 'films.name', 'films.web_name', 'films.duration')
                    ->first();

        // Trying to access a film that does not exist
        if (!$film) {
            return redirect()->route('bookings.films');
        }

        $scheduled_films = DB::table('scheduled_films')
                            ->join('theatres','scheduled_films.theatre_id','theatres.id')
                            ->join('cinemas','theatres.cinema_id','cinemas.id')
                            ->where('scheduled_films.film_id', $film->id)
                            ->where('cinemas.id', $current_cinema->id ?? null)
                            ->select(
                                'scheduled_films.id', 'theatres.number', 'scheduled_films.show_datetime', 'scheduled_films.tickets', 'scheduled_films.tickets_remaining'
                            )->where('scheduled_films.show_datetime', '>', CarbonImmutable::now()->tz('africa/johannesburg'))
                            ->get();

        return view('bookings.film', [
            'current_cinema' => $current_cinema,
            'cinemas' => $cinemas,
            'film' => $film,
            'scheduled_films' => $scheduled_films,
        ]);
    }

    ////////////////////////////////////////////////////////////
    // Save the booking in session before authentication
    public function save(Request $request)
    {
        // validate basic fields
        $validator = Validator::make($request->all(), [
            'scheduled_film_id' => 'required|int',
            'tickets' => 'nullable|int|min:1',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        // Store the schedule film id and tickets in session to remember while authenticating
        $request->session()->put('booking_details', [
            'id' => $request->scheduled_film_id,
            'tickets' => $request->tickets ?? 1,
        ]);

        return redirect()->route('booking.finish');
    }

    ////////////////////////////////////////////////////////////
    // Write the booking into the database
    public function finish(Request $request)
    {
        // Prevent saving the user booking if they do not have the correct session variable
        if (!$request->session()->has('booking_details.id')){
            return redirect()->route('bookings.films');
        }

        $booking_details = session()->get('booking_details');

        // Avoid creating multiple instances of date objects
        $datetime_now_tz = CarbonImmutable::now()->tz('africa/johannesburg');

        $scheduled_film = DB::table('scheduled_films')
                            ->where('scheduled_films.id', $booking_details['id'])
                            ->where('scheduled_films.show_datetime', '>', $datetime_now_tz)
                            ->join('theatres','scheduled_films.theatre_id','theatres.id')
                            ->join('cinemas','theatres.cinema_id','cinemas.id')
                            ->join('films','scheduled_films.film_id','films.id')
                            ->leftJoin('user_bookings','scheduled_films.id','user_bookings.scheduled_film_id')
                            ->select(
                                'scheduled_films.id', 'cinemas.id as cinema_id', 'cinemas.web_name as cinema_web_name', 'films.web_name as film_web_name', 
                                'scheduled_films.tickets', 'scheduled_films.tickets_remaining', 'user_bookings.id as user_booking_id'
                            )->first();

        // Trying to access a film that does not exist
        // Attempting to schedule a booking for the same time
        if (!$scheduled_film) {
            return redirect()->route('bookings.films')->withErrors('Invalid booking');
        } elseif (!empty($scheduled_film->user_booking_id)) {
            return redirect()->route('booking.film', ['cinema_web_name' => $scheduled_film->cinema_web_name ?? 0, 'film_web_name' => $scheduled_film->film_web_name])->withErrors('You already have a booking for this time');
        }

        // Current amount of ticket available
        $tickets = $scheduled_film->tickets_remaining ?? $scheduled_film->tickets;

        // Tickets left after booking
        $tickets_remaining = $tickets - $booking_details['tickets'];

        // Trying to book when there is not enough tickets left
        if ($tickets == 0) {
            return redirect()->route('booking.film', ['cinema_web_name' => $scheduled_film->cinema_web_name ?? 0, 'film_web_name' => $scheduled_film->film_web_name])->withErrors("There are no tickets left");
        } elseif ($tickets_remaining < 0 || $booking_details['tickets'] > $scheduled_film->tickets) {
            return redirect()->route('booking.film', ['cinema_web_name' => $scheduled_film->cinema_web_name ?? 0, 'film_web_name' => $scheduled_film->film_web_name])->withErrors("There are only {$tickets} tickets left");
        }

        // 1 char C reference number 4 digit cinema id and 11 random digits
        $reference_number = 'C' . sprintf('%04d', $scheduled_film->cinema_id) . sprintf('%011d', random_int(1,99999999999));

        // Rollback the transaction if the second insert query failed
        DB::beginTransaction();
        
        // Attempt insert of user booking and update the tickets left for the scheduled time
        try {
            DB::table('user_bookings')->insert([
                'user_id' => Auth::id(),
                'scheduled_film_id' => $booking_details['id'],
                'tickets' => $booking_details['tickets'],
                'reference_number' => $reference_number,
                'created_at' => $datetime_now_tz,
                'updated_at' => $datetime_now_tz,
            ]);

            DB::table('scheduled_films')
                    ->where('id', $booking_details['id'])
                    ->update(['tickets_remaining' => $tickets_remaining]);
        } catch (Exception $e){
            DB::rollback();
            return redirect()->route('booking.film', ['cinema_web_name' => $scheduled_film->cinema_web_name ?? 0, 'film_web_name' => $scheduled_film->film_web_name])->withErrors('Internal error, please try again in a few minutes');
        }

        DB::commit();
        
        // Keep the reference number in session for the next request
        $request->session()->flash('reference_number', $reference_number);
        // Forget the booking details to avoid reusing the session using url to reach this route
        $request->session()->forget('booking_details');

        return redirect()->route('booking.finished');
    }

    ////////////////////////////////////////////////////////////
    // Display the reference number to the user
    public function finished(Request $request)
    {
        // Keep the reference number in session for the next request
        $request->session()->keep('reference_number');

        $reference_number = $request->session()->get('reference_number');
       
        return view('bookings.finished', [
            'reference_number' => $reference_number,
        ]);
    }

}
