<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Auth;
use Carbon\CarbonImmutable;
use Exception;
use Validator;

class UserProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    ////////////////////////////////////////////////////////////
    // List the historical bookings made by the user
    public function history(Request $request)
    {

        $user_bookings = DB::table('user_bookings')
                            ->where('user_bookings.user_id', Auth::id())
                            ->join('scheduled_films','user_bookings.scheduled_film_id','scheduled_films.id')
                            ->join('films','scheduled_films.film_id','films.id')
                            ->select('user_bookings.id', 'films.name', 'scheduled_films.show_datetime', 'films.duration', 'user_bookings.tickets')
                            ->orderBy('scheduled_films.show_datetime', 'DESC')
                            ->get();

        return view('userprofile.history', [
            'user_bookings' => $user_bookings
        ]);
    }

    ////////////////////////////////////////////////////////////
    // Cancel the booking via a post request
    public function cancelBooking(Request $request)
    {
        // validate basic fields
        $validator = Validator::make($request->all(), [
            'id' => 'required|int',
        ]);

        $user_booking = DB::table('user_bookings')
                            ->where('user_bookings.user_id', Auth::id())
                            ->where('user_bookings.id', $request->id)
                            ->join('scheduled_films','user_bookings.scheduled_film_id','scheduled_films.id')
                            ->select('user_bookings.id', 'scheduled_films.id as scheduled_film_id', 'scheduled_films.show_datetime', 'user_bookings.tickets', 'scheduled_films.tickets_remaining')
                            ->first();

        // Trying to delete a booking that does not exist
        if (!$user_booking) {
            return redirect()->route('user.history')->withErrors("Invalid booking");
        }

        // Prevent deleting booking 1 hour before showing
        $start_date = CarbonImmutable::createFromFormat('Y-m-d H:i:s', $user_booking->show_datetime, 'Africa/Johannesburg')->subHours();

        if ($start_date->isPast()) {
            return redirect()->route('user.history')->withErrors("Cannot cancel booking an hour prior to start");
        }

        // get the number of tickets that will be free after cancelation
        $tickets_remaining = $user_booking->tickets + $user_booking->tickets_remaining;

        // Rollback the transaction if the second insert query failed
        DB::beginTransaction();

        // Delete user booking and revert the tickets remaining
        try {
            DB::table('user_bookings')->where('id', $request->id)->delete();

            DB::table('scheduled_films')
                    ->where('id', $user_booking->scheduled_film_id)
                    ->update(['tickets_remaining' => $tickets_remaining]);
        } catch (Exception $e) {
            return redirect()->route('user.history')->withErrors("Internal error, please try again in a few minutes");
        }

        DB::commit();

        return redirect()->route('user.history')->with(['success' => 'Booking canceled']);
    }
}
