@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">Select a Time</div>

                <div class="card-body">
                    @component('components.errors')
                    @endcomponent
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6 d-flex justify-content-start align-items-center">
                            @if(!empty($current_cinema))
                                {{ $current_cinema->name ?? "" }}
                            @else
                                Please select a cinema
                            @endif
                            </div>
                            <div class="col-sm-6 d-flex justify-content-end">
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Select Your Cinema
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        @foreach ($cinemas as $cinema)
                                            <a class="dropdown-item" href="{{ route('booking.film', ['cinema_web_name' => $cinema->web_name, 'film_web_name' => $film->web_name]) }}">{{ $cinema->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 py-4">
                        <div class="row">
                            <div class="border col-md-3 mb-3" style="height: 200px"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 font-weight-bold">{{ $film->name }}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-3  mb-3">Duration: {{ $film->duration }} Minutes</div>
                        </div>
                        @if(!empty($current_cinema))
                            @if($scheduled_films->isNotEmpty())
                            <form method="POST" action="{{ route('booking.save') }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="active">Time:</label>
                                        <select id="schedule_datetime" class="form-control" name="scheduled_film_id">
                                            @foreach($scheduled_films as $scheduled_film)
                                                <option data-tickets="{{ $scheduled_film->tickets_remaining ?? $scheduled_film->tickets }}" value='{{ $scheduled_film->id }}'>{{ $scheduled_film->show_datetime }} (Theatre {{ $scheduled_film->number }})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-2">
                                        <label for="tickets">Tickets (<span id="ticket"></span> remaining):</label>
                                        <input type="text" class="form-control" name="tickets">
                                    </div>
                                </div>
                                
                                <button type="submit" class="btn btn-success my-2 px-3">Book Time</button>
                            </form>
                            @else
                            <div>No schedules found for this movie at this cinema. Try another</div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    let tickets = $("#schedule_datetime > option:selected").data('tickets');
    $("#ticket").html(tickets);

    $(document).ready(function(){
        {{-- catch either droplist change --}}
        $("#schedule_datetime").change(function(){
            let tickets = $("#schedule_datetime > option:selected").data('tickets');
            $("#ticket").html(tickets);
        });
    });
</script>

@endsection
