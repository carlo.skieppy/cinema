@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">Select Your Film</div>

                <div class="card-body">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6 d-flex justify-content-start align-items-center">
                                {{-- <h3></h3> --}}
                                {{ $current_cinema->name ?? "All Cinemas" }}
                            </div>
                            <div class="col-sm-6 d-flex justify-content-end">
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Select Your Cinema
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        @foreach ($cinemas as $cinema)
                                            <a class="dropdown-item" href="{{ route('bookings.films', ['cinema_web_name' => $cinema->web_name]) }}">{{ $cinema->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                        @foreach($films as $film)
                            <div class="col-sm-12 col-md-6 col-lg-4 py-4">
                                <div class="border mb-3" style="height: 200px"></div>
                                <div class="font-weight-bold">{{ $film->name }}</div>
                                <div>Duration: {{ $film->duration }} Minutes</div>
                                <div><a class="btn btn-primary my-2 px-3" href="{{ route('booking.film', ['cinema_web_name' => $current_cinema->web_name ?? 0, 'film_web_name' => $film->web_name]) }}" role="button">Book Now</a></div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
