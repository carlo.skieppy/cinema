@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">Booking Completed</div>

                <div class="card-body">
                    <div>Your booking has been completed</div>
                    <div class="my-2">Reference number: {{ $reference_number }}</div>
                    <div><a class="btn btn-primary my-2 px-3" href="{{ route('bookings.films') }}" role="button">Return to films</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
