@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @component('components.errors')
                    @endcomponent
                    <div class="col-sm-12">
                        <div class="row">

                        @foreach($user_bookings as $user_booking)
                            <div class="col-sm-12 col-md-6 col-lg-4 py-4">
                                <div class="border mb-3" style="height: 200px"></div>
                                <div class="font-weight-bold">{{ $user_booking->name }}</div>
                                <div>Show Time: {{ $user_booking->show_datetime }}</div>
                                <div>Duration: {{ $user_booking->duration }} Minutes</div>
                                <div>Tickets: {{ $user_booking->tickets }}</div>
                                @if(!\Carbon\CarbonImmutable::createFromFormat('Y-m-d H:i:s', $user_booking->show_datetime, 'Africa/Johannesburg')->subHours()->isPast())
                                <button type="button" class="btn btn-xs btn-danger my-2 px-3 delete_booking" data-bookingid="{{ $user_booking->id }}">Cancel Booking</button>
                                @endif
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form method="POST" action="{{ route("user.history.cancel") }}" id="deleteForm">
    @csrf
    <input type="hidden" id="booking_id" name="id" value=""/>
</form>

<script type="text/javascript">
    $(document).ready(function(){
        $('.delete_booking').click(function(){
            if (confirm('Are you sure you want to delete this booking?') == true) {
                let booking_id = $(this).data('bookingid');
                $('#booking_id').val(booking_id);
                $("#deleteForm").submit();
            }
        });
    });
</script>

@endsection
