<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insertOrIgnore([
            [
                'id' => 1,
                'name' => 'testme',
                'email' => 'test@gmail.com',
                'password' => Hash::make('password'),
            ],[
                'id' => 2,
                'name' => 'testme2',
                'email' => 'test2@gmail.com',
                'password' => Hash::make('password'),
            ],[
                'id' => 3,
                'name' => 'testme3',
                'email' => 'test3@gmail.com',
                'password' => Hash::make('password'),
            ]
        ]);
    }
}
