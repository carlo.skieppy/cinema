<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Carbon\CarbonImmutable;

class ScheduledFilmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('scheduled_films')->insertOrIgnore([
            [
                'theatre_id' => 1,
                'film_id' => 1,
                'show_datetime' => CarbonImmutable::today()->addDays()->hour('11'),
            ],[
                'theatre_id' => 1,
                'film_id' => 2,
                'show_datetime' => CarbonImmutable::today()->addDays()->hour('16'),
            ],[
                'theatre_id' => 2,
                'film_id' => 3,
                'show_datetime' => CarbonImmutable::today()->addDays()->hour('11'),
            ],[
                'theatre_id' => 2,
                'film_id' => 4,
                'show_datetime' => CarbonImmutable::today()->addDays()->hour('17'),
            ],[
                'theatre_id' => 3,
                'film_id' => 3,
                'show_datetime' => CarbonImmutable::today()->addDays()->hour('11'),
            ],[
                'theatre_id' => 3,
                'film_id' => 2,
                'show_datetime' => CarbonImmutable::today()->addDays()->hour('18'),
            ],[
                'theatre_id' => 4,
                'film_id' => 1,
                'show_datetime' => CarbonImmutable::today()->addDays()->hour('11'),
            ],[
                'theatre_id' => 4,
                'film_id' => 3,
                'show_datetime' => CarbonImmutable::today()->addDays()->hour('19'),
            ],[
                'theatre_id' => 1,
                'film_id' => 1,
                'show_datetime' => CarbonImmutable::today()->addDays()->hour('13'),
            ],[
                'theatre_id' => 1,
                'film_id' => 2,
                'show_datetime' => CarbonImmutable::today()->addDays()->hour('18'),
            ],[
                'theatre_id' => 2,
                'film_id' => 3,
                'show_datetime' => CarbonImmutable::today()->addDays()->hour('13'),
            ],[
                'theatre_id' => 2,
                'film_id' => 4,
                'show_datetime' => CarbonImmutable::today()->addDays()->hour('19'),
            ],[
                'theatre_id' => 3,
                'film_id' => 3,
                'show_datetime' => CarbonImmutable::today()->addDays()->hour('13'),
            ],[
                'theatre_id' => 3,
                'film_id' => 2,
                'show_datetime' => CarbonImmutable::today()->addDays()->hour('20'),
            ],[
                'theatre_id' => 4,
                'film_id' => 1,
                'show_datetime' => CarbonImmutable::today()->addDays()->hour('13'),
            ],[
                'theatre_id' => 4,
                'film_id' => 3,
                'show_datetime' => CarbonImmutable::today()->addDays()->hour('21'),
            ],[
                'theatre_id' => 1,
                'film_id' => 1,
                'show_datetime' => CarbonImmutable::today()->addDays('2')->hour('13'),
            ],[
                'theatre_id' => 1,
                'film_id' => 2,
                'show_datetime' => CarbonImmutable::today()->addDays('2')->hour('18'),
            ],[
                'theatre_id' => 2,
                'film_id' => 3,
                'show_datetime' => CarbonImmutable::today()->addDays('2')->hour('13'),
            ],[
                'theatre_id' => 2,
                'film_id' => 4,
                'show_datetime' => CarbonImmutable::today()->addDays('2')->hour('19'),
            ],[
                'theatre_id' => 3,
                'film_id' => 3,
                'show_datetime' => CarbonImmutable::today()->addDays('2')->hour('13'),
            ],[
                'theatre_id' => 3,
                'film_id' => 2,
                'show_datetime' => CarbonImmutable::today()->addDays('2')->hour('20'),
            ],[
                'theatre_id' => 4,
                'film_id' => 1,
                'show_datetime' => CarbonImmutable::today()->addDays('2')->hour('13'),
            ],[
                'theatre_id' => 4,
                'film_id' => 3,
                'show_datetime' => CarbonImmutable::today()->addDays('2')->hour('21'),
            ],[
                'theatre_id' => 1,
                'film_id' => 1,
                'show_datetime' => CarbonImmutable::today()->addDays('2')->hour('13'),
            ]
        ]);
    }
}
