<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CinemaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cinemas')->insertOrIgnore([
            [
                'id' => 1,
                'name' => 'Awesome Cinema',
                'web_name' => 'awesome-cinema',
                'description' => 'best place ever',
                'addr1' => 'addr1 1',
                'addr2' => 'addr2 1',
                'city' => 'city 1',
                'zip' => '1111',
                'email' => 'test@cinemas1.com',
                'phone' => '123456789',
            ],[
                'id' => 2,
                'name' => '2nd Awesome Cinema',
                'web_name' => '2nd-awesome-cinema',
                'description' => '2nd best place ever',
                'addr1' => 'addr1 2',
                'addr2' => 'addr2 2',
                'city' => 'city 2',
                'zip' => '2222',
                'email' => 'test@cinemas2.com',
                'phone' => '987654321',
            ]
        ]);
    }
}
