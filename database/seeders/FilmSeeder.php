<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FilmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('films')->insertOrIgnore([
            [
                'id' => 1,
                'name' => 'Bestest Film 2',
                'web_name' => 'bestest-film-2',
                'duration' => 120,
                'active' => \CinemaDefs::ACTIVE,
            ],[
                'id' => 2,
                'name' => 'Running Out Of Time 5',
                'web_name' => 'running-out-of-time-5',
                'duration' => 125,
                'active' => \CinemaDefs::ACTIVE,
            ],[
                'id' => 3,
                'name' => 'Schrodingers Cat',
                'web_name' => 'schrodingers-cat',
                'duration' => 99,
                'active' => \CinemaDefs::ACTIVE,
            ],[
                'id' => 4,
                'name' => 'Riperonies',
                'web_name' => 'riperonies',
                'duration' => 110,
                'active' => \CinemaDefs::ACTIVE,
            ],[
                'id' => 5,
                'name' => 'Appelstrudel',
                'web_name' => 'appelstrudel',
                'duration' => 90,
                'active' => \CinemaDefs::ACTIVE,
            ]
        ]);
    }
}
