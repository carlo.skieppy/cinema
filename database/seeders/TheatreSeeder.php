<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TheatreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('theatres')->insertOrIgnore([
            [
                'id' => 1,
                'cinema_id' => 1,
                'number' => 1,
                'active' => \CinemaDefs::ACTIVE,
            ],[
                'id' => 2,
                'cinema_id' => 1,
                'number' => 2,
                'active' => \CinemaDefs::ACTIVE,
            ],[
                'id' => 3,
                'cinema_id' => 2,
                'number' => 1,
                'active' => \CinemaDefs::ACTIVE,
            ],[
                'id' => 4,
                'cinema_id' => 2,
                'number' => 2,
                'active' => \CinemaDefs::ACTIVE,
            ]
        ]);
    }
}
