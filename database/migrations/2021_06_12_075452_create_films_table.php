<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('web_name')->nullable();
            $table->string('description')->nullable();
            $table->unsignedInteger('duration')->nullable();
            $table->char('active',1)->default(\CinemaDefs::ACTIVE);
            $table->timestamps();
            
            $table->index('web_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
