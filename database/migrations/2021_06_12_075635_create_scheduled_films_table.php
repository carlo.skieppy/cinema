<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduledFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduled_films', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('theatre_id')->nullable();
            $table->unsignedInteger('film_id')->nullable();
            $table->timestamp('show_datetime')->nullable();
            $table->unsignedInteger('tickets')->nullable()->default(30);
            $table->unsignedInteger('tickets_remaining')->nullable();
            $table->timestamps();

            $table->foreign('theatre_id')->references('id')->on('theatres');
            $table->foreign('film_id')->references('id')->on('films');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduled_films');
    }
}
