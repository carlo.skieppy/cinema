<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('bookings.films');
});

// list film/flims
Route::get('/booking/films/{cinema_web_name?}', [App\Http\Controllers\BookingController::class, 'films'])->name('bookings.films');
Route::get('/booking/film/{cinema_web_name?}/{film_web_name?}', [App\Http\Controllers\BookingController::class, 'film'])->name('booking.film');

// save booking in session
Route::post('/booking/save', [App\Http\Controllers\BookingController::class, 'save'])->name('booking.save');

//login using laravels built in auth
Auth::routes();

Route::group(['middleware' => 'auth'], function(){
    // finish booking after register and show reference
    Route::get('/booking/finish', [App\Http\Controllers\BookingController::class, 'finish'])->name('booking.finish');
    Route::get('/booking/finished', [App\Http\Controllers\BookingController::class, 'finished'])->name('booking.finished');

    // user routes
    // page to show page to see all the films booked
    Route::get('/booking/history', [App\Http\Controllers\UserProfileController::class, 'history'])->name('user.history');
    Route::post('/booking/cancel', [App\Http\Controllers\UserProfileController::class, 'cancelBooking'])->name('user.history.cancel');
});
